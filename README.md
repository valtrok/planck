# Planck keymap and configuration

This repository contains my planck keyboard QMK configuration. Feel free to use it and modify it. It is made for a planck rev6 keyboard.

## Setup

Clone and make a link the keymap to your qmk installation folder

```
git clone https://gitlab.com/valtrok/planck.git
cd planck
./scripts/link_keymap.sh
```

Setup private_defs.c file with all private variables

```
./scripts/set_private_defs.sh
```

Generate keymap.c

```
./scripts/make_keymap.sh
```

Flash your keyboard

```
./scripts/flash.sh
```
