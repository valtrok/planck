#!/bin/bash

QMK_HOME=$(qmk config -ro user.qmk_home | cut -d= -f2)
KEYMAP_PATH=$QMK_HOME/keyboards/planck/keymaps/mine

ln -s $(pwd) $KEYMAP_PATH
